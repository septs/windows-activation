function Get-Links([string] $name) {
    $params = @{
        type="PackageFamilyName";
        url="$name";
        ring="RP";
        lang="en-US";
    }
    $response = Invoke-WebRequest `
        -Uri "https://store.rg-adguard.net/api/GetFiles" `
        -Method POST `
        -ContentType "application/x-www-form-urlencoded" `
        -Body $params `
        -UseBasicParsing
    $parser = New-Object -Com "HTMLFile"
    [string] $body = $response.Content
    $parser.Write([ref] $body)
    return $parser.getElementsByTagName('a')
}

function Add-PackageSet([string] $name) {
    Get-Links $name `
    | Where-Object { -not $_.textContent -match "\.BlockMap$" } `
    | ForEach-Object {
        $name = $_.textContent
        $path = $_.href
        Add-AppxPackage -Path $path -ErrorAction SilentlyContinue -ErrorVariable error
        if (-not $error) {
            Write-Host "Installed $name"
        }
    }
}

Add-PackageSet "Microsoft.DesktopAppInstaller_8wekyb3d8bbwe"
Add-PackageSet "Microsoft.StorePurchaseApp_8wekyb3d8bbwe"
Add-PackageSet "Microsoft.XboxIdentityProvider_8wekyb3d8bbwe"
Add-PackageSet "Microsoft.WindowsStore_8wekyb3d8bbwe"
