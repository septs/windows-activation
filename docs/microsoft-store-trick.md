# Microsoft Store Trick

## Restore Store on LTSC edition (Method 1)

Restore Store without an External Script on LTSC 2021

```powershell
Start-Process wsreset '-i' -Verb RunAs
```

## Restore Store on LTSC edition (Method 2)

```powershell
iwr "https://gitlab.com/septs/windows-activation/-/raw/main/install-store.ps1" -useb | iex
```

## Uninstall Store

```powershell
Get-AppxPackage -AllUser *WindowsStore* | Remove-AppxPackage
```

## Reinstall Store

```powershell
Get-AppxPackage -AllUsers *WindowsStore* | `
  ForEach-Object { Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml" }
```

## References

- [Add Store to Windows 10 Enterprise LTSC](https://github.com/kkkgo/LTSC-Add-MicrosoftStore) (LTSC 2019)
- [How to Remove and Reinstall Microsoft Store on Windows 11](https://www.makeuseof.com/remove-reinstall-microsoft-store-windows-11/)
- [Restore the Microsoft Store (Windows 11)](https://github.com/WOA-Project/SurfaceDuo-Guides/blob/main/RestoreMicrosoftStore.md)
