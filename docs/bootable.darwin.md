# Make a bootable Windows USB drive from a Mac

```console
$ # List of External USB disk
$ ./bootable.darwin.sh
Please select disk
/dev/disk4 (external, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:                                                   *32.0 GB    disk4

$ # Run Make Bootable Windows Installer Script on `disk4` from `Win10_22H2_English_x64v1.iso`
$ ./bootable.darwin.sh disk4 Win10_22H2_English_x64v1.iso
+ diskutil eraseDisk MS-DOS WINDOWS10 MBR disk4
Started erase on disk4
Unmounting disk
Creating the partition map
Waiting for partitions to activate
Formatting disk4s1 as MS-DOS (FAT) with name WINDOWS10
512 bytes per physical sector
/dev/rdisk4s1: 62378208 sectors in 1949319 FAT32 clusters (16384 bytes/cluster)
bps=512 spc=32 res=32 nft=2 mid=0xf8 spt=32 hds=255 hid=2048 drv=0x80 bsec=62408704 bspf=15230 rdcl=2 infs=1 bkbs=6
Mounting disk
Finished erase on disk4
+ hdiutil mount Win10_22H2_English_x64v1.iso -mountpoint /Volumes/WindowsInstaller
/dev/disk5                                           /Volumes/WindowsInstaller
++ stat -f %z /Volumes/WindowsInstaller/sources/install.wim
+ '[' 5181270654 -gt 3984588800 ']'
+ rsync --archive --human-readable --stats --exclude=sources/install.wim /Volumes/WindowsInstaller/ /Volumes/WINDOWS10/

Number of files: 991
Number of files transferred: 905
Total file size: 954.26M bytes
Total transferred file size: 954.26M bytes
Literal data: 954.26M bytes
Matched data: 0 bytes
File list size: 28468
File list generation time: 0.048 seconds
File list transfer time: 0.000 seconds
Total bytes sent: 954.45M
Total bytes received: 20.45K

sent 954.45M bytes  received 20.45K bytes  54.54M bytes/sec
total size is 954.26M  speedup is 1.00
+ wimlib-imagex split /Volumes/WindowsInstaller/sources/install.wim /Volumes/WINDOWS10/sources/install.svm 3800
Splitting WIM: 4903 MiB of 4903 MiB (100%) written, part 2 of 2
Finished splitting "/Volumes/WindowsInstaller/sources/install.wim"
+ hdiutil detach /Volumes/WindowsInstaller
"disk5" ejected.
```

## References

- [Make a bootable Windows 10 USB drive from a Mac](https://alexlubbock.com/bootable-windows-usb-on-mac)
  [web.archive.org/~](https://web.archive.org/web/20230407181419/https://alexlubbock.com/bootable-windows-usb-on-mac)
