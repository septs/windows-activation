# References

## List of Tools

- [Rufus](https://rufus.ie) - Create bootable USB drives the easy way
- [UUP dump](https://uupdump.net) - Download UUP files from Windows Update servers with ease.
- [MediaCreationTool.bat](https://github.com/AveYo/MediaCreationTool.bat)
- [Microsoft Activation Scripts](https://massgrave.dev) (MAS)

## List of KMS Server (Mainland China)

- <https://moe.best/kms.html>
- <https://blog.03k.org/post/kms.html>
- <https://kms.cangshui.net>
- <https://www.coolhub.top/tech-articles/kms_list.html>

## List of Articles

- [如何将 Windows 10 Enterprise LTSC 2021 评估版升级到完整版](https://www.cnblogs.com/CnKker/p/15705769.html) (Chinese)
- [KMS client activation and product keys](https://learn.microsoft.com/windows-server/get-started/kms-client-activation-keys)
- [Upgrade and conversion options for Windows Server](https://learn.microsoft.com/windows-server/get-started/upgrade-conversion-options)
- [Windows Setup Edition Configuration and Product ID Files (EI.cfg and PID.txt)](https://learn.microsoft.com/windows-hardware/manufacture/desktop/windows-setup-edition-configuration-and-product-id-files--eicfg-and-pidtxt)
- [Microsoft Store Trick](microsoft-store-trick.md)
