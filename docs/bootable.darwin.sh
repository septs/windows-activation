#!/bin/bash
set -euo pipefail
TARGET_DISK="$1"
IMAGE_FILE="$2"
LABEL='WINDOWS10'
INSTALLER='WindowsInstaller'
INSTALLER_WIM="/Volumes/$INSTALLER/sources/install.wim"
INSTALLER_SVM="/Volumes/$LABEL/sources/install.svm"
RSYNC_OPTIONS=(--archive --human-readable --stats)
if [[ $OSTYPE != 'darwin'* ]]; then
  echo "The script only macOS available"
  exit 1
elif [[ ! -x "$(which rsync)" ]]; then
  echo "rsync command not found."
  echo "see https://command-not-found.com/rsync"
  exit 1
elif [[ ! -x "$(which wimlib-imagex)" ]]; then
  echo "wimlib-imagex command not found."
  echo "see https://command-not-found.com/wimlib-imagex"
  exit 1
fi
if [[ -z "$TARGET_DISK" ]]; then
  echo "Please select disk"
  diskutil list external
  exit 1
fi
IMAGE_SIZE="$(stat -f "%z" "$IMAGE_FILE")"
IMAGE_TYPE="$(file --brief --mime-type "$IMAGE_FILE")"
if [[ "$IMAGE_TYPE" != "application/x-iso9660-image" ]]; then
  echo "Please provide Windows ISO image"
  exit 1
fi
TARGET_DISK_INFO="$(mktemp)"
diskutil information -plist "$TARGET_DISK" > "$TARGET_DISK_INFO"
TARGET_SIZE="$(/usr/libexec/PlistBuddy -c "Print TotalSize" "$TARGET_DISK_INFO")"
TARGET_REMOVABLE="$(/usr/libexec/PlistBuddy -c "Print Removable" "$TARGET_DISK_INFO")"
if [[ "$IMAGE_SIZE" -gt "$TARGET_SIZE" ]]; then
  FORMATTED_TARGET="$(awk "BEGIN { print ($TARGET_SIZE / (1000 ** 3)) }")"
  FORMATTED_IMAGE="$(awk "BEGIN { print ($IMAGE_SIZE / (1000 ** 3)) }")"
  echo "The capacity of the target disk ($FORMATTED_TARGET GB) is smaller than the image size ($FORMATTED_IMAGE GB)"
  exit 1
elif [[ "$TARGET_REMOVABLE" != "true" ]]; then
  echo "The disk is not removable"
  exit 1
fi
set -x
diskutil eraseDisk MS-DOS "$LABEL" MBR "$TARGET_DISK"
hdiutil mount "$IMAGE_FILE" -mountpoint "/Volumes/$INSTALLER"
if [ "$(stat -f "%z" "$INSTALLER_WIM")" -gt "$((1024 * 1024 * 3800))" ]; then
  rsync "${RSYNC_OPTIONS[@]}" --exclude=sources/install.wim "/Volumes/$INSTALLER/" "/Volumes/$LABEL/"
  wimlib-imagex split "$INSTALLER_WIM" "$INSTALLER_SVM" 3800
else
  rsync "${RSYNC_OPTIONS[@]}" "/Volumes/$INSTALLER/" "/Volumes/$LABEL/"
fi
hdiutil detach "/Volumes/$INSTALLER"
