# Windows Activation

## Quick Start (PowerShell)

```powershell
iwr aka.pw/kms.ps1 -useb | iex
```

## Availability

| Edition Name                        |  Test  |
| ----------------------------------- | :----: |
| [Windows 10 Customer]               | [MAS]  |
| [Windows 11 Customer]               | [MAS]  |
| [Windows 10 Enterprise]             | TESTED |
| [Windows 11 Enterprise]             | TESTED |
| [Windows Server 2022]               | TESTED |
| [Windows Server 2019]               | TESTED |
| [Windows Server 2019 Essentials]    | TESTED |
| [Windows Server 2016]               | TESTED |
| [Windows Server 2016 Essentials]    | TESTED |
| [Windows Server 2012 R2]            | TESTED |
| [Windows Server 2012 R2 Essentials] | TESTED |

[mas]: https://massgrave.dev "Microsoft Activation Scripts"
[windows 10 customer]: https://microsoft.com/software-download/windows10
[windows 11 customer]: https://microsoft.com/software-download/windows11
[windows 10 enterprise]: https://microsoft.com/evalcenter/download-windows-10-enterprise
[windows 11 enterprise]: https://microsoft.com/evalcenter/download-windows-11-enterprise
[windows server 2022]: https://microsoft.com/evalcenter/download-windows-server-2022
[windows server 2019]: https://microsoft.com/evalcenter/download-windows-server-2019
[windows server 2019 essentials]: https://microsoft.com/evalcenter/download-windows-server-2019-essentials
[windows server 2016]: https://microsoft.com/evalcenter/download-windows-server-2016
[windows server 2016 essentials]: https://microsoft.com/evalcenter/download-windows-server-2016-essentials
[windows server 2012 r2]: https://microsoft.com/evalcenter/download-windows-server-2012-r2
[windows server 2012 r2 essentials]: https://microsoft.com/evalcenter/download-windows-server-2012-r2-essentials

## Example

```console
PS C:\Windows\system32> iwr aka.pw/kms.ps1 -useb | iex
> slmgr /rilc
Re-installing license files ...
...
License files re-installed successfully.

> slmgr /upk
Uninstalled product key successfully.

> slmgr /ckms
Key Management Service machine name cleared successfully.

> slmgr /cpky
Product key from registry cleared successfully.

> slmgr /ipk M7XTQ-FN8P6-TTKYV-9D4CC-J462D
Installed product key M7XTQ-FN8P6-TTKYV-9D4CC-J462D successfully.

> slmgr /skms kms.03k.org
Key Management Service machine name set to kms.03k.org successfully.

> slmgr /ato
Activating Windows(R), EnterpriseS edition (32d2fab3-e4a8-42c2-923b-4bf4fd13e6ee) ...
Product activated successfully.

> slmgr /xpr
Windows(R), EnterpriseS edition:
    Volume activation will expire 12/19/2023 4:04:02 AM

> slmgr /dlv
Software licensing service version: 10.0.19041.3086

Name: Windows(R), EnterpriseS edition
Description: Windows(R) Operating System, VOLUME_KMSCLIENT channel
Activation ID: 32d2fab3-e4a8-42c2-923b-4bf4fd13e6ee
Application ID: 55c92734-d682-4d71-983e-d6ec3f16059f
Extended PID: 03612-04250-000-000002-03-2052-19044.0000-1732023
Product Key Channel: Volume:GVLK
Installation ID: 381451087572307680680098739586467669010376978762605110069369442
Partial Product Key: J462D
License Status: Licensed
Volume activation expiration: 259200 minute(s) (180 day(s))
Remaining Windows rearm count: 2
Remaining SKU rearm count: 1001
Trusted time: 6/22/2023 4:04:03 AM
Configured Activation Type: All

Most recent activation information:
Key Management Service client information
    Client Machine ID (CMID): 1c3e29da-a556-4c84-93e0-afe31987c37f
    Registered KMS machine name: kms.03k.org:1688
    KMS machine IP address: 120.77.242.231
    KMS machine extended PID: 03612-00206-558-138906-03-2052-17763.0000-3232019
    Activation interval: 43200 minutes
    Renewal interval: 43200 minutes
    KMS host caching is enabled
```

## References

- [Make a bootable Windows USB drive from a Mac](docs/bootable.darwin.md)
- [References](docs/references.md)
