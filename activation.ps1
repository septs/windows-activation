#requires -RunAsAdministrator
& {
    $identity = [Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()
    $role = [Security.Principal.WindowsBuiltInRole]::Administrator

    if (-not $identity.IsInRole($role)) {
        Write-Warning "Insufficient permissions for the script."
        Write-Warning "Open PowerShell console as administrator and re-run this script."
        break
    }
}

Set-StrictMode -Version 3.0
$ErrorActionPreference = "Stop"

$baseUrl = "https://gitlab.com/septs/windows-activation/-/raw/main"
$system32 = "${env:windir}\System32"

$editionId = & {
    if ($env:KMS_EDITION) {
        $env:KMS_EDITION
    } else {
        Get-ItemPropertyValue `
            -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion" `
            -Name "CompositionEditionID"
    }
}
$channel = & {
    if ($env:KMS_CHANNEL) {
        $env:KMS_CHANNEL
    } else {
        "Volume"
    }
}

Add-Type -TypeDefinition @"
using System;
using System.Runtime.InteropServices;

public static class ProductKey {
    [DllImport("pkeyhelper.dll", EntryPoint = "GetEditionIdFromName", CharSet = CharSet.Unicode)]
    static extern int getEditionId(string name, ref int id);
    [DllImport("pkeyhelper.dll", EntryPoint = "SkuGetProductKeyForEdition", CharSet = CharSet.Unicode)]
    static extern int getProductKey(int id, string channel, ref IntPtr key, ref IntPtr pfn);

    public static string Get(string name, string channel) {
        // Available Channel Values: "Retail", "OEM", "Volume", "Volume:MAK" and "Volume:GVLK"
        int id = 0;
        IntPtr key = new IntPtr();
        IntPtr pfn = new IntPtr();
        getEditionId(name, ref id);
        getProductKey(id, channel, ref key, ref pfn);
        return Marshal.PtrToStringUni(key);
    }
}
"@

function slmgr([parameter(ValueFromRemainingArguments = $true)][string[]] $params) {
    # https://learn.microsoft.com/windows-server/get-started/activation-slmgr-vbs-options
    Write-Host ">" "slmgr" @params
    cscript.exe "$system32\slmgr.vbs" "//NoLogo" @params
}

function dism([parameter(ValueFromRemainingArguments = $true)][string[]] $params) {
    # https://learn.microsoft.com/windows-hardware/manufacture/desktop/what-is-dism
    Write-Host ">" "dism" @params
    dism.exe "/English" "/NoRestart" @params
}

function Get-KMSHost() {
    if ($env:KMS_HOST) {
        return $env:KMS_HOST
    }
    try {
        $hosts = Invoke-WebRequest -Uri "https://www.coolhub.top/assets/data/kms.json" | ConvertFrom-JSON
        return $hosts[0].Host
    } catch {
        return "kms.03k.org"
    }
}

function Convert-ServerProduct([string] $editionId) {
    $productKey = [ProductKey]::Get($editionId, $channel)

    slmgr /skms $server
    dism /Online "/Set-Edition:${editionId}" "/ProductKey:${productKey}" /AcceptEula
}

function Convert-DesktopProduct([string] $editionId) {
    $major = [Environment]::OSVersion.Version.Major
    $skuPath = "${editionId}-${major}.zip"

    if ($PSScriptRoot) {
        $skuFile = "${PSScriptRoot}\skus\${skuPath}"
    } else {
        $skuFile = "$(New-TemporaryFile).zip"
        Invoke-WebRequest -Uri "${baseUrl}/skus/${skuPath}" -OutFile "${skuFile}"
    }

    Expand-Archive `
        -Path $skuFile `
        -DestinationPath "${system32}\spp\tokens\skus" `
        -Force

    slmgr /rilc
    slmgr /upk
    slmgr /ckms
    slmgr /cpky

    Set-Service LicenseManager -StartupType Automatic
    Start-Service LicenseManager
    Set-Service wuauserv -StartupType Automatic
    Start-Service wuauserv
}

function Invoke-Activate([string] $editionId) {
    $productKey = [ProductKey]::Get($editionId, $channel)
    $kmsHost = Get-KMSHost
    slmgr /skms $kmsHost
    slmgr /ipk $productKey
    slmgr /ato
    slmgr /xpr
    slmgr /dlv
}

if ($editionId -match "Eval$") {
    $edition = $editionId -replace ("Eval$", "")
    if ($edition -match "^Server") {
        Convert-ServerProduct $edition
        break
    } else {
        Convert-DesktopProduct $edition
    }
}

Invoke-Activate $editionId
